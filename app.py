from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from services.content_generator import title_generator, dynamic_generator, hashtag_generator
import os
import uvicorn


class Item(BaseModel):
    type_content: str
    company: str
    product: str
    industry: str
    goal: str
    type_of_video: str
    keywords: str


app = FastAPI()
origins = [
    "http://localhost:3000",
    "http://localhost:8000",
    "http://remobay.asia",
    "http://reelsights.app",
    "http://52.76.57.212/",
    "http://52.76.57.212/content-generator",
    "http://localhost:3000/content-generator",
    "http://remobay.asia:8000/content-generator",
    "http://0.0.0.0:8000",
    "http://localhost:8000/content-generator",
    "http://0.0.0.0:8000/content-generator",
    "http://remobay.asia:8000"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}


@app.post('/content-generator')
async def content_generator(item: Item):
    if item.type_content == 'title':
        content = title_generator(item.company,
                                  item.product,
                                  item.industry,
                                  item.goal,
                                  item.type_of_video,
                                  item.keywords)
    elif item.type_content == 'hashtag':
        content = hashtag_generator(item.company,
                                    item.product,
                                    item.industry,
                                    item.goal,
                                    item.type_of_video,
                                    item.keywords)
    else:
        content = dynamic_generator(item.type_content,
                                    item.company,
                                    item.product,
                                    item.industry,
                                    item.goal,
                                    item.type_of_video,
                                    item.keywords
                                    )
    return content


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8000)
