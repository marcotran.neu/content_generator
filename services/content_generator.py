import openai

openai.api_key = ''


def title_generator(company, product, industry, goal, type_of_video, keywords):
    response = openai.Completion.create(
        model="text-davinci-002",
        prompt="Create 5 video titles for a Youtube video following the criteria below:\n"
               f"Product: {product}\n"
               f"Industry: {industry}\n"
               f"Company: {company}\n"
               f"Type of video: {type_of_video}\n"
               f"Keywords: {keywords}\n"
               f"Goal: {goal}\n\n",
        temperature=0.5,
        max_tokens=4000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return {'status': 200,
            'content': response['choices'][0]['text']}


def hashtag_generator(company, product, industry, goal, type_of_video, keywords):
    response = openai.Completion.create(
        model="text-davinci-002",
        prompt="Create 5 video hashtags for a Youtube video following the criteria below:\n"
               f"Product: {product}\n"
               f"Industry: {industry}\n"
               f"Company: {company}\n"
               f"Type of video: {type_of_video}\n"
               f"Keywords: {keywords}\n"
               f"Goal: {goal}\n\n",
        temperature=0.5,
        max_tokens=4000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return {'status': 200,
            'content': response['choices'][0]['text']}


def dynamic_generator(type_content, company, product, industry, goal, type_of_video, keywords):
    response = openai.Completion.create(
        model="text-davinci-002",
        prompt=f"Create a video {type_content} for a Youtube video following the criteria below:\n"
               f"Product: {product}\n"
               f"Industry: {industry}\n"
               f"Company: {company}\n"
               f"Type of video: {type_of_video}\n"
               f"Keywords: {keywords}\n"
               f"Goal: {goal}\n\n",
        temperature=0.5,
        max_tokens=4000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return {'status': 200,
            'content': response['choices'][0]['text']}


def content_generator(country, industry, product_line, keywords, age, gender, platform):
    response = openai.Completion.create(
        model="text-davinci-002",
        prompt=f"write creative {platform} content following the criteria below:\n"
               f"Product line: {product_line}\n"
               f"Industry: {industry}\n"
               f"Country: {country}\n"
               f"Age: {age}\n"
               f"Keywords/hashtags: {keywords}\n"
               f"Gender: {gender}\n\n",
        temperature=0.5,
        max_tokens=4000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return {'status': 200,
            'content': response['choices'][0]['text']}


def script_generator(company, product, industry, location, type_of_video, keywords,
                     number_of_actors, relationship_between_actors, music):
    """
    for Tools 5 and 9 need to make API
    :param company:
    :param product:
    :param industry:
    :param location:
    :param type_of_video:
    :param keywords:
    :param number_of_actors:
    :param relationship_between_actors:
    :param music:
    :return:
    """
    response = openai.Completion.create(
        model="text-davinci-002",
        prompt="Create a video script for a Youtube marketing video following the criteria below\n"
               f"Product: {product}\n"
               f"Industry: {industry}\n"
               f"Company: {company}\n"
               f"Type of video: {type_of_video}\n"
               f"Keyword/hashtags: {keywords}\n"
               f"Number of actors: {number_of_actors}\n"
               f"Relationship between actors: {relationship_between_actors}\n"
               f"Music: {music}\n"
               f"Location: {location}\n\n",
        temperature=0.5,
        max_tokens=4000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return {'status': 200,
            'content': response['choices'][0]['text']}


def video_title_generator(company, product, industry, location, type_of_video, keywords,
                          number_of_actors, relationship_between_actors, music):
    """
    for Tool 9 need to make api
    :param company:
    :param product:
    :param industry:
    :param location:
    :param type_of_video:
    :param keywords:
    :param number_of_actors:
    :param relationship_between_actors:
    :param music:
    :return:
    """
    response = openai.Completion.create(
        model="text-davinci-002",
        prompt="Create 5 video titles for a Youtube video following the criteria below:\n"
               f"Product: {product}\n"
               f"Industry: {industry}\n"
               f"Company: {company}\n"
               f"Type of video: {type_of_video}\n"
               f"Keyword/hashtags: {keywords}\n"
               f"Number of actors: {number_of_actors}\n"
               f"Relationship between actors: {relationship_between_actors}\n"
               f"Music: {music}\n"
               f"Location: {location}\n\n",
        temperature=0.5,
        max_tokens=4000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return {'status': 200,
            'content': response['choices'][0]['text']}


def video_description_generator(company, product, industry, location, type_of_video, keywords,
                                number_of_actors, relationship_between_actors, music):
    """
    tool 9 need to make api
    :param company:
    :param product:
    :param industry:
    :param location:
    :param type_of_video:
    :param keywords:
    :param number_of_actors:
    :param relationship_between_actors:
    :param music:
    :return:
    """
    response = openai.Completion.create(
        model="text-davinci-002",
        prompt="Create a video descriptions for a Youtube video following the criteria below:\n"
               f"Product: {product}\n"
               f"Industry: {industry}\n"
               f"Company: {company}\n"
               f"Type of video: {type_of_video}\n"
               f"Keyword/hashtags: {keywords}\n"
               f"Number of actors: {number_of_actors}\n"
               f"Relationship between actors: {relationship_between_actors}\n"
               f"Music: {music}\n"
               f"Location: {location}\n\n",
        temperature=0.5,
        max_tokens=4000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return {'status': 200,
            'content': response['choices'][0]['text']}


def video_call_to_action_generator(company, product, industry, location, type_of_video, keywords,
                                   number_of_actors, relationship_between_actors, music):
    """
    tool 9 need to make api
    :param company:
    :param product:
    :param industry:
    :param location:
    :param type_of_video:
    :param keywords:
    :param number_of_actors:
    :param relationship_between_actors:
    :param music:
    :return:
    """
    response = openai.Completion.create(
        model="text-davinci-002",
        prompt="Create a video call to action for a Youtube video following the criteria below:\n"
               f"Product: {product}\n"
               f"Industry: {industry}\n"
               f"Company: {company}\n"
               f"Type of video: {type_of_video}\n"
               f"Keyword/hashtags: {keywords}\n"
               f"Number of actors: {number_of_actors}\n"
               f"Relationship between actors: {relationship_between_actors}\n"
               f"Music: {music}\n"
               f"Location: {location}\n\n",
        temperature=0.5,
        max_tokens=4000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return {'status': 200,
            'content': response['choices'][0]['text']}


def video_music_suggest(company, product, industry, location, type_of_video, keywords,
                        number_of_actors, relationship_between_actors, music):
    """
    tool 9 need to make api
    :param company:
    :param product:
    :param industry:
    :param location:
    :param type_of_video:
    :param keywords:
    :param number_of_actors:
    :param relationship_between_actors:
    :param music:
    :return:
    """
    response = openai.Completion.create(
        model="text-davinci-002",
        prompt="Suggest music video for a Youtube video following the criteria below:\n"
               f"Product: {product}\n"
               f"Industry: {industry}\n"
               f"Company: {company}\n"
               f"Type of video: {type_of_video}\n"
               f"Keyword/hashtags: {keywords}\n"
               f"Number of actors: {number_of_actors}\n"
               f"Relationship between actors: {relationship_between_actors}\n"
               f"Music: {music}\n"
               f"Location: {location}\n\n",
        temperature=0.5,
        max_tokens=4000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return {'status': 200,
            'content': response['choices'][0]['text']}
